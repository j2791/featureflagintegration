package com.sajal.ff4j;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class })
public class Ff4jSpringBootExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ff4jSpringBootExampleApplication.class, args);
	}

}
