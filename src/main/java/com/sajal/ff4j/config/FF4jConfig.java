package com.sajal.ff4j.config;

import org.ff4j.FF4j;
import org.ff4j.core.Feature;
import org.ff4j.core.FlippingStrategy;
import org.ff4j.strategy.time.ReleaseDateFlipStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FF4jConfig {

    public static final String TRANSACTION_FEATURE = "TransactionFeature";
    public static final Boolean TRANSACTION_FEATURE_STATUS = Boolean.TRUE;

    @Bean
    public FF4j ff4j() {
        FF4j ff4j = new FF4j();
        Feature helloFeature = new Feature(TRANSACTION_FEATURE);
        helloFeature.setEnable(TRANSACTION_FEATURE_STATUS);
        ff4j.createFeature(helloFeature);
        return ff4j;
    }
}
