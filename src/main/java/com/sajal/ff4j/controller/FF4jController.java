package com.sajal.ff4j.controller;

import com.sajal.ff4j.config.FF4jConfig;
import com.sajal.ff4j.dto.Product;
import com.sajal.ff4j.service.FF4jService;
import org.ff4j.FF4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FF4jController {

    private final FF4jService service;
    private final FF4j ff4j;

    public FF4jController(FF4jService service, FF4j ff4j) {
        this.service = service;
        this.ff4j = ff4j;
    }

    @GetMapping("/products")
    public List<Product> showAvailableProducts() {
        if (ff4j.check(FF4jConfig.TRANSACTION_FEATURE)) {
            return service.getAllDiscountedProducts();
        } else {
            return service.getAllProducts();
        }
    }
}
