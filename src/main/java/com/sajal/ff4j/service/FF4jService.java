package com.sajal.ff4j.service;

import com.sajal.ff4j.dto.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FF4jService {

    public List<Product> getAllProducts() {
        return Stream.of(new Product(1, "mobile", 50000),
                new Product(2, "headphone", 2000),
                new Product(3, "watch", 14999)
                , new Product(4, "glass", 999)
        ).collect(Collectors.toList());
    }

    public List<Product> getAllDiscountedProducts() {
        List<Product> orderListAfterDiscount = new ArrayList<>();
        this.getAllProducts().forEach(order -> {
            order.setPrice(order.getPrice() - (order.getPrice() * 5 / 100));
            orderListAfterDiscount.add(order);
        });
        return orderListAfterDiscount;
    }
}
